package me.ingels.games.snake.screens;

import com.badlogic.gdx.*;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.*;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import me.ingels.games.snake.SnakeGame;

import java.awt.*;

public class GameScreen extends ScreenAdapter {
    private static final String TAG = GameScreen.class.getName();

    private class BodyPart {
        private int x, y;
        private Sprite sprite;

        public BodyPart(Sprite sprite) {
            this.sprite = sprite;
        }

        public BodyPart(Sprite sprite, int x, int y) {
            this.sprite = sprite;
            this.x = x;
            this.y = y;
        }

        public void updatePosition(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public void draw(Batch batch) {
            if(this.x != snakeX || this.y != snakeY) {
                sprite.setPosition(this.x, this.y);
                sprite.draw(batch);
            }
        }
    }

    private static final int RIGHT = 0;
    private static final int LEFT = 1;
    private static final int UP = 2;
    private static final int DOWN = 3;
    private static final float MOVE_TIME = 0.5F;
    private static final float MOVE_TIME_SLOW = 1.0f;
    private static final float MOVE_TIME_MEDIUM = 0.5f;
    private static final float MOVE_TIME_FAST = 0.25f;
    private static final int SNAKE_SPEED = 32;
    private static final String appleText = "Pommes";
    private static final String gameOverText = "Game Over!";
    private static final String pressSpaceBarText = "Appuyez sur la barre d'espace pour rejouer";
    private static final String gameLevelText = "Niveau";
    private static final String readyText = "Ready!";
    private static final int WORLD_WIDTH = 640;
    private static final int WORLD_HEIGHT = 480;
    private static final int[] gameSpeedTable = {35, 20, 10, 0};

    private enum GAME_STATE {INIT, PLAYING, GAME_OVER};

    private SpriteBatch batch;
    private Color bgColor;
    private Sprite snakeHead;
    private Sprite snakeBody;
    private Sprite apple;
    private Texture background;
    private Sprite arrowLeft;
    private Sprite arrowRight;
    private Sprite arrowUp;
    private Sprite arrowDown;
    private int snakeX = 0, snakeY = 0;
    private int snakePrevX, snakePrevY;
    private int snakeDirection = RIGHT;
    private int snakeOppositeDirection = LEFT;
    private int appleX, appleY;
    private boolean appleAvailable = false;
    private GAME_STATE gameState = GAME_STATE.INIT;
    private BitmapFont alternateFont;
    private BitmapFont alternateFontBig;
    private GlyphLayout appleScoreLayout;
    private GlyphLayout gameOverLayout;
    private GlyphLayout spaceBarMsgLayout;
    private GlyphLayout gameTimeLayout;
    private GlyphLayout readyLayout;
    private int numberApples = 0;
    private Viewport viewport;
    private Camera camera;
    private float gameSpeed = MOVE_TIME_SLOW;
    private float timer;
    private int gameLevel = 1;
    private Timer myTimer;
    private Timer appleEnableTimer;
    private ShapeRenderer shapeRenderer;
    private TextureAtlas spriteSheet;
    private Sprite currentArrow = arrowRight;
    private ParticleEffect starEffect;
    private boolean fadingOutAppleSwitch = false;
    private Timer explodeAppleTimer;
    private float appleAlphaFactor = 1f;

    // To handle the snake body parts
    private Array<BodyPart> bodyParts = new Array<BodyPart>();

    private final SnakeGame game;

    public GameScreen(SnakeGame game) {
        this.game = game;
    }

    @Override
    public void show() {
        Gdx.app.setLogLevel(Application.LOG_DEBUG);

        myTimer = new Timer();
        myTimer.scheduleTask(new Timer.Task() {
            @Override
            public void run() {
                gameState = GAME_STATE.PLAYING;
            }
        }, 3);

        // Create a timer to delay the apparition of a new apple
        appleEnableTimer = new Timer();

        // Create a timer used for exploding the apple when hit by the snake
        explodeAppleTimer = new Timer();

        // Create the camera
        camera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        // Set the camera at the center of the world
        camera.position.set(WORLD_WIDTH / 2, WORLD_HEIGHT / 2, 0);
        // Recalculate the transformation and view matrix
        camera.update();

        // Create the viewport
        viewport = new FitViewport(WORLD_WIDTH, WORLD_HEIGHT, camera);

        // Create the sprite batch for drawing
        batch = new SpriteBatch();

        // Open the sprite sheet and get the sprites
        spriteSheet = new TextureAtlas("snakegameassets.atlas");
        snakeHead = spriteSheet.createSprite("snakehead");
        snakeBody = spriteSheet.createSprite("snakebody");
        apple = spriteSheet.createSprite("apple");
        arrowLeft = spriteSheet.createSprite("arrow-left");
        arrowLeft.setAlpha(0.25f);
        arrowRight = spriteSheet.createSprite("arrow-right");
        arrowRight.setAlpha(0.25f);
        arrowUp = spriteSheet.createSprite("arrow-up");
        arrowUp.setAlpha(0.25f);
        arrowDown = spriteSheet.createSprite("arrow-down");
        arrowDown.setAlpha(0.25f);
        bgColor = Color.BLACK;
        background = new Texture(Gdx.files.internal("game-background.jpg"));

        // Create a shape renderer for drawing the grid.
        shapeRenderer = new ShapeRenderer();

        // Create the fonts for text display
        alternateFont = new BitmapFont(Gdx.files.internal("NocturnalHand/NocturnalHand.fnt"), Gdx.files.internal("NocturnalHand/NocturnalHand.png"), false);
        alternateFontBig = new BitmapFont(Gdx.files.internal("NocturnalHandBig/NocturnalHandBig.fnt"), Gdx.files.internal("NocturnalHandBig/NocturnalHandBig.png"), false);
        appleScoreLayout = new GlyphLayout(alternateFont, appleText);
        gameOverLayout = new GlyphLayout(alternateFontBig, gameOverText);
        spaceBarMsgLayout = new GlyphLayout(alternateFont, pressSpaceBarText);
        gameTimeLayout = new GlyphLayout(alternateFont, gameLevelText);
        readyLayout = new GlyphLayout(alternateFont, readyText);

        Gdx.app.debug(TAG, "readyText width: " + String.valueOf(readyLayout.width) + ", height: " + String.valueOf(readyLayout.height));

        // Load start particle effect
        starEffect = new ParticleEffect();
        starEffect.load(Gdx.files.internal("particles/yellow-star.sfx"), Gdx.files.internal("particles"));
        starEffect.setPosition(50, 50);
        starEffect.start();

        // Set initial game speed (delay between two snake moves).
        timer = gameSpeed;

        // Place first apple to start the game
        checkAndPlaceApple();
    }

    @Override
    public void dispose() {
        batch.dispose();
        alternateFont.dispose();
        alternateFontBig.dispose();
        starEffect.dispose();
    }

    @Override
    public void render(float delta) {
        clearScene();
        drawBackground();

        switch(gameState) {
            case INIT:
                drawReadyText(batch);
                break;

            case PLAYING:
                timer -= delta;
                queryInput();
                handleSnakeMovements();
                handleAppleEating();
		        drawScene();
                break;

            case GAME_OVER:
                game.score = numberApples;
                game.setScreen(new GameOverScreen(this.game));
                /*
                gameOver(batch);
                if(Gdx.input.isKeyPressed(Input.Keys.SPACE)) doRestart();
                 */
                break;
        }
        updateScore(batch);
    }

    private void handleAppleEating() {
        if (checkAppleCollision()) {
            // Prepare to place a new apple around 1 second later, just to let the fading out effect
            // of the current apple to be done.
            appleEnableTimer.scheduleTask(new Timer.Task() {
                @Override
                public void run() {
                    checkAndPlaceApple();
                }
            }, 0.9f);

            // There is no more apple to check collision with. Now we just animate the apple fading out
            // and when done, we will place a new apple and make it available to check again collisions
            // with the snake.
            appleAvailable = false;

            // Create a new body part to the snake.
            BodyPart body = new BodyPart(snakeBody, snakeX, snakeY);
            // Add that new part to the snake.
            bodyParts.insert(0, body);
            
            // When the apple is hit, start animate its fading
            fadingOutAppleSwitch = true;
            // Start a timer to animate the apple explosion during a limited time. That animation will
            // last 500 ms and after, we reset the apple sprite values to their default ones.
            explodeAppleTimer.scheduleTask(new Timer.Task() {
                @Override
                public void run() {
                    fadingOutAppleSwitch = false;
                    apple.setScale(1f, 1f);
                    appleAlphaFactor = 1f;
                    apple.setAlpha(1f);
                }
            }, 0.500f);
        }

        // If fading out apple effect is on-going, we animate it…
        if(fadingOutAppleSwitch == true) {
            apple.scale(1f + Gdx.graphics.getDeltaTime() / 1000f);
            apple.setAlpha(appleAlphaFactor);
            appleAlphaFactor /= 1.15f;
        }
    }

    private void handleSnakeMovements() {
        if (timer <= 0) {
            // Reset the timer to its starting value
            timer = updateGameTimer();
            // Manage snake movements
            moveSnake();
            // Check if snake head goes out of screen bounds
            checkForOutOfBounds();
            // Update snake body parts position
            updateBodyPartsPosition();
            // Check if snake hit itself
            if(checkSnakeBodyCollision()) gameState = GAME_STATE.GAME_OVER;

            snakeHead.setPosition(snakeX, snakeY);
        }
    }

    // Accelerate the game depending on the number of apples.
    private float updateGameTimer() {
        if(numberApples >= 35) {
            if(gameLevel < 4) gameLevel++;
            return(0.15f);
        }
        if(numberApples >= 20) {
            if(gameLevel < 3) gameLevel++;
            return(0.25f);
        }
        if(numberApples >= 10) {
            if(gameLevel < 2) gameLevel++;
            return(0.5f);
        }
        return(1.0f);
    }

    private void checkForOutOfBounds() {
        if(snakeX >= viewport.getWorldWidth()) {
            snakeX = 0;
        }
        if(snakeX < 0) {
            snakeX = (int)viewport.getWorldWidth() - SNAKE_SPEED;
        }
        if(snakeY >= viewport.getWorldHeight()) {
            snakeY = 0;
        }
        if(snakeY < 0) {
            snakeY = (int)viewport.getWorldHeight() - SNAKE_SPEED;
        }
    }

    private void moveSnake() {
        // Store snake head position before moving
        snakePrevX = snakeX;
        snakePrevY = snakeY;

        // Update snake head position
        switch(snakeDirection) {
            case RIGHT: {
                snakeX += SNAKE_SPEED;
                return;
            }
            case LEFT: {
                snakeX -= SNAKE_SPEED;
                return;
            }
            case UP: {
                snakeY += SNAKE_SPEED;
                return;
            }
            case DOWN: {
                snakeY -= SNAKE_SPEED;
                return;
            }
        }
    }

    private void queryInput() {
        boolean leftKeyPressed = Gdx.input.isKeyPressed(Input.Keys.LEFT);
        boolean rightKeyPressed = Gdx.input.isKeyPressed(Input.Keys.RIGHT);
        boolean upKeyPressed = Gdx.input.isKeyPressed(Input.Keys.UP);
        boolean downKeyPressed = Gdx.input.isKeyPressed(Input.Keys.DOWN);

        if(leftKeyPressed) updateSnakeDirection(LEFT, snakeOppositeDirection);
        if(rightKeyPressed) updateSnakeDirection(RIGHT, snakeOppositeDirection);
        if(upKeyPressed) updateSnakeDirection(UP, snakeOppositeDirection);
        if(downKeyPressed) updateSnakeDirection(DOWN, snakeOppositeDirection);
    }

    private boolean updateSnakeDirection(int newDirection, int oppositeDirection) {
        // Check if the new requested direction is different than the current opposite direction.
        // In such a case, the new direction is not taken into account which prevents the snake
        // from slithering back on himself.
        if(oppositeDirection != newDirection) {
            // Take the new requested direction as the current one
            snakeDirection = newDirection;
            // Compute the opposite direction. Display an arrow in the middle of the screen to show the direction.
            switch(newDirection) {
                case LEFT: snakeOppositeDirection = RIGHT; currentArrow = arrowLeft; break;
                case RIGHT: snakeOppositeDirection = LEFT; currentArrow = arrowRight; break;
                case UP: snakeOppositeDirection = DOWN; currentArrow = arrowUp; break;
                case DOWN: snakeOppositeDirection = UP; currentArrow = arrowDown; break;
            }

            // Fire an action 500 ms later to hide the arrow displayed in the middle of the screen.
            myTimer.scheduleTask(new Timer.Task() {
                @Override
                public void run() {
                    currentArrow = null;
                }
            }, 0.5f);

            // Return true meaning that the snake direction has changed
            return true;
        }
        // Return false meaning that nothing changed
        return false;
    }

    private void checkAndPlaceApple() {
        boolean status;

        if(!appleAvailable) {
            do {
                appleX = MathUtils.random((int)viewport.getWorldWidth() / SNAKE_SPEED - 1) * SNAKE_SPEED;
                appleY = MathUtils.random((int)viewport.getWorldHeight() / SNAKE_SPEED - 1) * SNAKE_SPEED;
                appleAvailable = true;

                // Select again a new apple position until it is not at snake head or body position.
                status = false;
                if(appleX == snakeX && appleY == snakeY)
                    status = true;
                if(bodyParts.size > 0) {
                    for (BodyPart b : bodyParts) {
                        if (appleX == b.x && appleY == b.y) {
                            status = true;
                        }
                    }
                }
            }
            while(status == true);

            // Draw star effect at new apple position
            starEffect.setPosition(appleX, appleY);
            starEffect.start();

        }
    }

    private boolean checkAppleCollision() {
        if(appleAvailable && !fadingOutAppleSwitch && appleX == snakeX && appleY == snakeY) {
            numberApples++;
            return(true);
        }
        return(false);
    }

    private void updateBodyPartsPosition() {
        if(bodyParts.size > 0) {
            BodyPart body = bodyParts.removeIndex(0);
            body.updatePosition(snakePrevX, snakePrevY);
            bodyParts.add(body);

            // Gdx.app.debug(TAG, "Moving one part to position (" + String.valueOf(snakePrevX / 32) + ";" + String.valueOf(snakePrevY / 32) + ")");
        }
    }

    private void clearScene() {
        Gdx.gl.glClearColor(bgColor.getRed(),
                bgColor.getGreen(),
                bgColor.getBlue(),
                bgColor.getAlpha());
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
    }

    private void drawScene() {
        batch.setProjectionMatrix(camera.projection);
        batch.setTransformMatrix(camera.view);

        starEffect.update(Gdx.graphics.getDeltaTime());

        batch.begin();
        if(currentArrow != null) {
            drawArrow(currentArrow);
        }

        // Draw the snake head
        snakeHead.draw(batch);

        // Draw the snake body parts
        for (BodyPart b : bodyParts) {
            b.draw(batch);
        }
        // Draw apple if available or if it is exploding because eaten by the snake
        if(appleAvailable || fadingOutAppleSwitch) {
            apple.setPosition(appleX, appleY);
            apple.draw(batch);
        }

        starEffect.draw(batch);
        batch.end();
    }

    private void drawBackground()
    {
        batch.setProjectionMatrix(camera.projection);
        batch.setTransformMatrix(camera.view);
        batch.begin();
        batch.draw(background, 0, 0);
        batch.end();
    }

    private void drawGrid() {
        shapeRenderer.setProjectionMatrix(camera.projection);
        shapeRenderer.setTransformMatrix(camera.view);

        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
            shapeRenderer.setColor(1, 1, 1, 1);
            for(int i = 0; i < viewport.getWorldHeight(); i++) {
                shapeRenderer.line(0, i * 32, viewport.getWorldWidth() - 1, i * 32);
            }
            for(int i = 0; i < viewport.getWorldWidth() / 32; i++) {
                shapeRenderer.line(i * 32, 0, i * 32, viewport.getWorldHeight() - 1);
            }
        shapeRenderer.end();
    }

    private boolean checkSnakeBodyCollision() {
        for(BodyPart b: bodyParts) {
            if(b.x == snakeX && b.y == snakeY)
                return true;
        }
        return false;
    }

    private void gameOver(Batch batch) {
        int w = (int)viewport.getWorldWidth();
        int h = (int)viewport.getWorldHeight();

        batch.begin();
        alternateFontBig.draw(batch, gameOverText,
                (w - gameOverLayout.width) / 2,
                (h - gameOverLayout.height / 2) / 2);
        alternateFont.draw(batch, pressSpaceBarText, (w - spaceBarMsgLayout.width) / 2,
                (h - gameOverLayout.height) / 2 + gameOverLayout.height - 20);
        batch.end();
    }

    private void updateScore(Batch batch) {
        int h = (int)viewport.getWorldHeight();
        int w = (int)viewport.getWorldWidth();

        batch.begin();
        alternateFont.draw(batch, appleText, 20, h - (int)(appleScoreLayout.height * 1.5));
        alternateFont.draw(batch, String.valueOf(numberApples), 20 + (int)appleScoreLayout.width + 10, h - (int)(appleScoreLayout.height * 1.5));
        alternateFont.draw(
                batch,
                gameLevelText,
                w - gameTimeLayout.width - 40,
                h - (int)(gameTimeLayout.height * 1.5)
        );
        alternateFont.draw(batch, String.valueOf(gameLevel), w - 20, h - (int)(gameTimeLayout.height * 1.5));
        batch.end();
    }

    private void drawSpecialEffects() {
        starEffect.update(Gdx.graphics.getDeltaTime());
        batch.begin();
        // Draw particle effects
        starEffect.draw(batch);
        batch.end();
    }

    private void drawReadyText(Batch batch) {
        batch.begin();
        alternateFontBig.draw(batch, readyText, (viewport.getWorldWidth() - readyLayout.width) / 2,
                (viewport.getWorldHeight() + readyLayout.height) / 2,
                readyLayout.width, Align.center, false);
        batch.end();
    }

    private void drawArrow(Sprite arrow) {
        int w = (int)viewport.getWorldWidth();
        int h = (int)viewport.getWorldHeight();
        arrow.setPosition((w - arrow.getWidth()) / 2, (h - arrow.getHeight()) / 2);
        arrow.draw(batch);
    }

    private void doRestart() {
        // Return to the startup screen
        game.setScreen(new StartupScreen(game));
        /*
        gameState = GAME_STATE.PLAYING;
        snakeX = 0;
        snakeY = 0;
        snakeDirection = RIGHT;
        snakeOppositeDirection = LEFT;
        bodyParts.clear();
        appleAvailable = false;
        numberApples = 0;
        timer = MOVE_TIME;
        gameLevel = 0;
        currentArrow = arrowRight;
        checkAndPlaceApple();
        */
    }
}   // End of GameScreen class
