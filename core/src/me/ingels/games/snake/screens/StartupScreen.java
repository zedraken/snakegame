package me.ingels.games.snake.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.FillViewport;
import me.ingels.games.snake.SnakeGame;

public class StartupScreen extends ScreenAdapter {
    private static final String TAG = StartupScreen.class.getName();

    private Stage stage;
    private FillViewport viewport;
    private Texture bgTexture;
    private Texture btPlayTexture;
    private Texture btPlayPressedTexture;
    private Texture btQuitTexture;
    private Texture btQuitPressedTexture;
    private ImageButton btPlay;
    private ImageButton btQuit;

    private final SnakeGame game;

    public StartupScreen(SnakeGame game) {
        this.game = game;
    }

    public void show() {
        Gdx.app.log(TAG, "show()");

        viewport = new FillViewport(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        stage = new Stage(viewport);
        Gdx.input.setInputProcessor(stage);

        bgTexture = new Texture(Gdx.files.internal("game-background.jpg"));
        Image background = new Image(bgTexture);
        stage.addActor(background);

        btPlayTexture = new Texture(Gdx.files.internal("button_play.png"));
        btPlayPressedTexture = new Texture(Gdx.files.internal("button_play_pressed.png"));
        btPlay = new ImageButton(new TextureRegionDrawable(btPlayTexture), new TextureRegionDrawable(btPlayPressedTexture));
        btPlay.setPosition(viewport.getWorldWidth() / 2, viewport.getWorldHeight() * 3 / 5, Align.center);
        btPlay.addListener(new ActorGestureListener(){
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                super.tap(event, x, y, count, button);
                game.setScreen(new GameScreen(game));
            }
        });
        stage.addActor(btPlay);

        btQuitTexture = new Texture(Gdx.files.internal("button_quit.png"));
        btQuitPressedTexture = new Texture(Gdx.files.internal("button_quit_pressed.png"));
        btQuit = new ImageButton(new TextureRegionDrawable(btQuitTexture), new TextureRegionDrawable(btQuitPressedTexture));
        btQuit.setPosition(viewport.getWorldWidth() / 2, viewport.getWorldHeight() * 2 / 5, Align.center);
        btQuit.addListener(new ActorGestureListener(){
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                super.tap(event, x, y, count, button);

            }
        });
        stage.addActor(btQuit);
    }

    public void render(float delta) {
        stage.act(delta);
        stage.draw();
    }

    public void dispose() {
        Gdx.app.log("StartupScreen", "dispose()");
        btPlayTexture.dispose();
        btPlayPressedTexture.dispose();
        btQuitTexture.dispose();
        btQuitPressedTexture.dispose();
        stage.dispose();
    }
}
