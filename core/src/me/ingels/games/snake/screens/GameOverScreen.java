package me.ingels.games.snake.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.FillViewport;
import me.ingels.games.snake.SnakeGame;

public class GameOverScreen extends ScreenAdapter {
    private static final String TAG = ScreenAdapter.class.getName();

    private FillViewport viewport;
    private Stage stage;
    private BitmapFont mainFont;
    private BitmapFont bigFont;
    private GlyphLayout gameOverLayout;
    private GlyphLayout scoreLayout;
    private SpriteBatch batch;
    private SnakeGame theGame;
    private ImageButton btBack;
    private Texture btBackTexture;
    private Texture btBackPressedTexture;

    public GameOverScreen(SnakeGame game) {
        this.theGame = game;
    }

    @Override
    public void show() {
        viewport = new FillViewport(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        batch = new SpriteBatch();
        stage = new Stage(viewport, batch);
        Gdx.input.setInputProcessor(stage);

        mainFont = new BitmapFont(Gdx.files.internal("NocturnalHand/NocturnalHand.fnt"));
        bigFont = new BitmapFont(Gdx.files.internal("NocturnalHandBig/NocturnalHandBig.fnt"));
        gameOverLayout = new GlyphLayout(bigFont, "Game Over");
        scoreLayout = new GlyphLayout(mainFont, "Votre score: " + String.valueOf(theGame.score));

        btBackTexture = new Texture(Gdx.files.internal("home_button.png"));
        btBack = new ImageButton(new TextureRegionDrawable(btBackTexture));
        btBack.setPosition(viewport.getWorldWidth() / 2, viewport.getWorldHeight() * 1 / 5, Align.center);
        btBack.addListener(new ActorGestureListener() {
                               @Override
                               public void tap(InputEvent event, float x, float y, int count, int button) {
                                   super.tap(event, x, y, count, button);
                                   theGame.setScreen(new StartupScreen(theGame));
                               }
                           });

        stage.addActor(btBack);

        Gdx.app.log(TAG, "gameOverLayout: " + String.valueOf(gameOverLayout.width) + "x" + String.valueOf(gameOverLayout.height));
        Gdx.app.log(TAG, "screen size: " + String.valueOf(Gdx.graphics.getWidth()) + "x" + String.valueOf(Gdx.graphics.getHeight()));
    }

    @Override
    public void render(float delta) {
        clearScreen();
        drawText();
        stage.act(delta);
        stage.draw();
    }

    public void clearScreen() {
        Gdx.gl.glClearColor(0.1f, 0.5f, 0.005f, 1.0f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
    }

    public void drawText() {
        batch.begin();
        bigFont.draw(batch, "Game Over",
                (Gdx.graphics.getWidth() - gameOverLayout.width) / 2,
                (Gdx.graphics.getHeight() - gameOverLayout.height) / 2 + gameOverLayout.height);

        mainFont.draw(batch, "Votre score: " + String.valueOf(theGame.score),
                (Gdx.graphics.getWidth() - scoreLayout.width) / 2,
                (Gdx.graphics.getHeight() - scoreLayout.height) / 5 * 2 + (scoreLayout.height / 2));
        batch.end();
    }
}
