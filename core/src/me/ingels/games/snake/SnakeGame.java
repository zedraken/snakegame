package me.ingels.games.snake;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import me.ingels.games.snake.screens.GameOverScreen;
import me.ingels.games.snake.screens.GameScreen;
import me.ingels.games.snake.screens.StartupScreen;

public class SnakeGame extends Game {

	public int score;

	public SnakeGame() {
		score = 0;
	}

	@Override
	public void create () {
		setScreen(new StartupScreen(this));
		// setScreen(new GameOverScreen(this));
	}

}
